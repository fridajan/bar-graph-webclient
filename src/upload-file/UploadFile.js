import React, { Component } from 'react';
import './UploadFile.css';
import Result from './Result';

class UploadFile extends Component {

    constructor(props) {
        super(props);
        this.state = {
            file: null,
            errorMessage: null,
            graphData: []
        };
        this.onChange = this.onChange.bind(this);
        this.onSubmit = this.onSubmit.bind(this);
        this.uploadFile = this.uploadFile.bind(this);
    }

    onChange(e) {
        this.setState({ file: e.target.files[0], errorMessage: null });
    }

    onSubmit(e) {
        e.preventDefault();

        if (this.state.file) {
            this.uploadFile(this.state.file).then((response) => {
                if(response.errorMessage) {
                    this.setState({ errorMessage: response.errorMessage});
                    return;
                }
                this.setState({ graphData: JSON.parse(response)});
            });
        } else {
            this.setState({ errorMessage: 'Please select a file to upload', graphData: [] });
        }
    }

    uploadFile(file) {
        const apiUrl = 'http://localhost:1293/api/UploadFile';

        const formData = new FormData();
        formData.append('file', file)

        return fetch(apiUrl, {
            method: 'POST',
            body: formData
        })
        .then(response => response.json());
    }

    render() {
        return (
            <div className="upload-file" onSubmit={this.onSubmit}>
                <form>
                    <input type="file" onChange={this.onChange} />
                    <button type="submit">Upload</button>
                </form>
                
                {this.state.errorMessage ? <p className="upload-file-error-message">{this.state.errorMessage}</p> : ''}

                {this.state.graphData ? <Result data={this.state.graphData} /> : ''}

            </div>
        );
    }
}

export default UploadFile;