import React, { Component } from 'react';
import './UploadFile.css';

class Result extends Component {

    render() {
        return (
            <div className="result">
                <p>Upload result:</p>
                <table>
                    <tr>
                        <th>Name</th>
                        <th>Size</th>
                        <th>Color</th>
                    </tr>
                    {this.props.data.map((bar, index) =>
                        <tr key={index}>
                            <td>{bar.Name}</td>
                            <td>{bar.Color}</td>
                            <td>{bar.Size}</td>
                        </tr>
                        )}
                </table>
            </div>
        );
    }
}

export default Result;