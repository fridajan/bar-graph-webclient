import React, { Component } from 'react';
import UploadFile from './../upload-file/UploadFile';
import './App.css';


class App extends Component {

  render() {
    return (
      <div className="App">
        <header className="App-header">
          <h1 className="App-title">Welcome!</h1>
        </header>
        <div className="App-intro">
          <UploadFile />
        </div>
      </div>
    );
  }
}

export default App;
