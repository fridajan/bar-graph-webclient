This project was bootstrapped with [Create React App](https://github.com/facebookincubator/create-react-app).

You can find a guide to Create React App [here](https://github.com/facebookincubator/create-react-app/blob/master/packages/react-scripts/template/README.md).

## Getting started

Install [nodejs](https://nodejs.org/en/)

Install [yarn](https://yarnpkg.com/)

Run the command 

  yarn

to install dependencies locally. 

Run the command

  yarn start

to start the application. It will start a local webserver on [http://localhost:3000](http://localhost:3000)

Visual Studio Code is recommended as IDE. 